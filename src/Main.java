public class Main {
    public static void main(String[] args) {
        int matriculationBookNumber = 0x123abc; // Номер зачётной книжки
        long phoneNumber = 89501234567L; // Номер телефона
        int lastTwoNoZeroDigits = 0b1000011; // Последние две ненулевые цифры номера телефона
        int lastFourNoZeroDigits = 010727; // Последние четыре ненулевые цифры номера телефона
        int remainder = (15-1) % 26 + 1; // Увеличенный на единицу остаток при делении уменьшенного на единицу номера студента в журнале группы (15) на 26
        char symbol = (char)((int)'A' + remainder - 1); // Символ английского алфавита в верхнем регистре, номер которого соответствует найденному ранее значению
    }
}